
#ifndef BYTE_CBUF_H_
#define BYTE_CBUF_H_

#include<stdint.h>
#include<stdbool.h>

typedef struct _byte_cbuf_ {
	uint8_t *buffer;
	uint16_t size;

	uint8_t write_index;
	uint8_t read_index;
	uint16_t length;

	uint32_t init_magic_number;
}byte_cbuf_t;

void byte_cbuf_init(byte_cbuf_t *cbuf,uint8_t *buffer,uint16_t size);
bool byte_cbuf_push(byte_cbuf_t *cbuf,uint8_t data);
bool byte_cbuf_pop(byte_cbuf_t *cbuf,uint8_t *data);
bool byte_cbuf_is_empty(byte_cbuf_t *cbuf);
void byte_cbuf_print_details(byte_cbuf_t *cbuf);
#endif /* BYTE_CBUF_H_ */
