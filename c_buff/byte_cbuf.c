#include<stdio.h>
#include<stdbool.h>
#include "byte_cbuf.h"

#define INIT_MAGIC_NUMBER 0XDEADBEEF
/////////////////////////////////////////Public Functions
void byte_cbuf_init(byte_cbuf_t *cbuf,uint8_t *buffer,uint16_t size)
{
	if(!cbuf || !buffer)
		return;

	if(cbuf->init_magic_number==INIT_MAGIC_NUMBER)
		return;

	cbuf->buffer=buffer;
	cbuf->size=size;

	cbuf->length=0;
	cbuf->read_index=0;
	cbuf->write_index=0;

	cbuf->init_magic_number=INIT_MAGIC_NUMBER;

}
bool byte_cbuf_push(byte_cbuf_t *cbuf,uint8_t data)
{
	if(!cbuf)
		return false;

	if(cbuf->init_magic_number!=INIT_MAGIC_NUMBER)
		return false; //not initialized

	if(cbuf->length==cbuf->size)
		return false; //no space available;

	cbuf->buffer[cbuf->write_index]=data;
	cbuf->write_index = ((cbuf->write_index+1)%cbuf->size);
	cbuf->length++;

	return true;

}

bool byte_cbuf_pop(byte_cbuf_t *cbuf,uint8_t *data)
{
	if(!cbuf || !data)
		return false;

	if(cbuf->init_magic_number!=INIT_MAGIC_NUMBER)
		return false;

	if(cbuf->length==0)
		return false;

	*data = cbuf->buffer[cbuf->read_index];
	cbuf->buffer[cbuf->read_index]=0;
	cbuf->read_index=((cbuf->read_index+1)%cbuf->size);
	cbuf->length--;

	return true;
}

bool byte_cbuf_is_empty(byte_cbuf_t *cbuf)
{
	if(!cbuf)
		return false;

	if(cbuf->init_magic_number!=INIT_MAGIC_NUMBER)
		return false;

	if(cbuf->length==0)
		return true;

	return false;
}

void byte_cbuf_print_details(byte_cbuf_t *cbuf)
{
	if(!cbuf)
		return;

	printf("\n");
	printf("Length:%d (size %d)\r\n",cbuf->length,cbuf->size);

	for(uint8_t i=0;i<cbuf->size;i++)
	{
		if(i==cbuf->read_index)
			printf("   R");
		else
			printf("   ");
	}
	printf("\n");

	for(uint8_t i=0;i<cbuf->size;i++)
	{
		printf("%4d ",cbuf->buffer[i]);
	}

	printf("\n");

	for(uint8_t i=0;i<cbuf->size;i++)
	{
		if(i==cbuf->write_index)
			printf("   W");
		else
			printf("   ");
	}
	printf("\n");

}
