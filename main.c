#include<stdio.h>
#include<stdint.h>
#include "c_buff/byte_cbuf.h"

#define BUFFER_SIZE 10


static uint8_t _buffer[BUFFER_SIZE];
static byte_cbuf_t _cbuf;


static void _add_data(uint8_t data);
static void _remove_data(void);

////////////////////////////////////////Public Functions
int main()
{
	byte_cbuf_init(&_cbuf, _buffer, BUFFER_SIZE);
	byte_cbuf_print_details(&_cbuf);

	_add_data(10);
	_add_data(15);
	_add_data(20);

	byte_cbuf_print_details(&_cbuf);

	_remove_data();
	_remove_data();

	byte_cbuf_print_details(&_cbuf);
	return 0;
}
//////////////////////////////////////////Private Functions
static void _add_data(uint8_t data)
{
	bool result;

	result = byte_cbuf_push(&_cbuf, data);
	if(result==false)
		printf("Failed to add data \r\n");
	else
		printf("Adding : %d\r\n",data);
}

static void _remove_data(void)
{
	bool result;
	uint8_t data;

	result = byte_cbuf_pop(&_cbuf,&data);
	if(result==false)
		printf("Failed to remove data \r\n");
}
